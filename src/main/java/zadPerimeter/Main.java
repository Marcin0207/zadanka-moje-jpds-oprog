package zadPerimeter;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        float diameter;
        float perimeter1;
        float perimeter2;

        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj średnicę");
        diameter = sc.nextFloat();

        perimeter1 = Calculation.perimeterCalcPi(diameter);
        perimeter2 = Calculation.perimeterCalcMath(diameter);
        System.out.println("Srednica z pi: " +perimeter1);
        System.out.println("Srednica z math: " +perimeter2);


    }
}
